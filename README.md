# Vue

#### 介绍
Vue练习册

#### 软件架构
软件架构说明


#### 安装教程

1.   **下载对应包**   
    `npm install`
2.   **启动项目**   
    `npm run dev`
3.   **启动如遇到以下错误** 
（Vue与vue-template-compiler版本不统一导致)
    > Vue packages version mismatch:

    > vue@2.5.2 (C:\Vue\node_modules\vue\dist\vue.runtime.common.js)

    > vue-template-compiler@2.6.11 (C:\Vue\node_modules\vue-template-compiler\package.json)

    > This may cause things to work incorrectly. Make sure to use the same version for both.

    > If you are using vue-loader@>=10.0, simply update vue-template-compiler.

    > If you are using vue-loader@<10.0 or vueify, re-installing vue-loader/vueify should bump vue-template-compiler to the latest.
    
     **解决方法：** 

    第一步：执行`npm uninstall vue-template-compiler`

    第二步：执行`npm install vue-template-compiler@报错中提示的vue的版本号`

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
