import fetch from '@/utils/request'
import { data } from 'autoprefixer'

// Axios POST请求 发送接收参：Params   Get请求：data

export function getWangYiNews(data) {
    return fetch({
        url: '/HCenter/getWangYiNews',
        method: 'post',
        params: data
    })
}
export function GetDBInfo(data) {
    return fetch({
        url: '/HCenter/GetDBInfo',
        method: 'post',
        data: data
    })
}
//获取所有controller信息
export function GetControllerInfo() {
    return fetch({
        url: '/api/GetControllerInfo',
        method: 'get',
    })
}
//根据controllerid获取controller下面的所有api集合
export function GetActionBuildList(id) {
    return fetch({
        url: '/api/GetActionBuildList',
        method: 'get',
        params: { ID: id, WhereType: "Controller" },
    })
}
//获取所有的Point类型
export function GetPointType() {
    return fetch({
        url: '/api/GetPointType',
        method: 'get',
    })
}
//获取单个api的信息
// export function GetActionBuildInfo(id) {
//     return fetch({
//         url: '/HCenter/GetActionBuildInfo?id=' + id,
//         method: 'get',
//     })
// }
//根据buildID获取CommunicationPoint信息
// export function GetCommunicationPointList(id) {
//     return fetch({
//         url: '/HCenter/GetCommunicationPointList?buildid=' + id,
//         method: 'get',
//     })
// }
//获取所有出参信息
// export function GetActionOutputList(id) {
//     return fetch({
//         url: '/HCenter/GetActionOutputList?buildid=' + id,
//         method: 'get',
//     })
// }
//根据CommunicationID获取sqlTpl信息
// export function GetActionSqlTpl(id) {
//     return fetch({
//         url: '/HCenter/GetActionSqlTpl?id=' + id,
//         method: 'get',
//     })
// }
//根据CommunicationID获取本地DLL信息
// export function GetActionInvokeMethod(id) {
//     return fetch({
//         url: '/HCenter/GetActionInvokeMethod?id=' + id,
//         method: 'get',
//     })
// }
//根据CommunicationID获取RemoteApi信息
// export function GetActionRemoteApi(id) {
//     return fetch({
//         url: '/HCenter/GetActionRemoteApi?id=' + id,
//         method: 'get',
//     })
// }
//根据CommunicationID获取CSScript信息
// export function GetActionInvokeCSScript(id) {
//     return fetch({
//         url: '/HCenter/GetActionInvokeCSScript?id=' + id,
//         method: 'get',
//     })
// }
// 根据CommunicationID获取input信息
// export function GetActionFieldList(id) {
//     return fetch({
//         url: '/HCenter/GetActionFieldList?id=' + id,
//         method: 'get',
//     })
// }
// export function GetActionCondotionList(id) {
//     return fetch({
//         url: '/HCenter/GetActionCondotionList?id=' + id,
//         method: 'get',
//     })
// }
//保存控制器
export function SaveControllerInfo(data) {
    return fetch({
        url: '/api/savecontroller',
        method: 'post',
        data: data,
    })
}
//删除控制器
export function DeleteControllerInfo(id) {
    return fetch({
        url: '/api/DeleteControllerInfo',
        method: 'post',
        data: { id: id }
    })
}
//删除接口信息
export function DeleteBuildInfo(data) {
    return fetch({
        url: '/api/DeleteBuildInfo',
        method: 'post',
        data: data
    })
}
//更新ActionBuild
export function UpDataActionBuild(data) {
    return fetch({
        url: '/api/UpDataActionBuild',
        method: 'post',
        data: data,
    })
}
//入参集合保存
export function SaveActionFieldList(data) {
    return fetch({
        url: '/api/SaveActionFieldList',
        method: 'post',
        data: data,
    })
}
//入参删除
export function DeleteActionFieldList(data) {
    return fetch({
        url: '/api/DeleteActionFieldList',
        method: 'post',
        data: data,
    })
}
//出参集合保存
export function SaveActionOutPutList(data) {
    return fetch({
        url: '/api/SaveActionOutPutList',
        method: 'post',
        data: data,
    })
}
//CommunicationPoint更新
export function UpDataCommunicationPoint(data) {
    return fetch({
        url: '/api/UpDataCommunicationPoint',
        method: 'post',
        data: data,
    })
}
//pointtype类信息更新
export function UpDataPointTypeInfo(type, entity) {
    return fetch({
        url: '/api/UpDataPointTypeInfo',
        method: 'post',
        data: { type: type, entity: entity },
    })
}
//新建api
export function NewActionBuild(data) {
    return fetch({
        url: '/api/NewActionBuild',
        method: 'post',
        data: data,
    })
}
//获取action整体
export function GetActionBuildEntity(id) {
    return fetch({
        url: '/api/GetActionBuildInfo',
        method: 'get',
        params: { ID: id, WhereType: "Builder" },
    })
}
//查询条件保存
export function SaveConditionList(data) {
    return fetch({
        url: '/api/SaveConditionList',
        method: 'post',
        data: data,
    })
}
//出参删除
// export function DeleteActionOutPut(data,id,keyword) {
    export function DeleteActionOutPut(data) {
    return fetch({
        url: '/api/DeleteActionOutPut',
        method: 'post',
        data: data
        // data: { output: data, ID: id , keyword:keyword},
    })
}
//条件查询
export function DeleteCondition(data) {
    return fetch({
        url: '/api/DeleteCondition',
        method: 'post',
        data: data,
    })
}