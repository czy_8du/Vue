import axios from 'axios';
import eml, { Message } from 'element-ui'
import router from '@/router/index';

// axios.defaults.baseURL = 'https://api.apiopen.top';//API请求地址
// axios.defaults.timeout = 5000;//超时时间 5S

const ServiceReq = axios.create({
    //baseURL : 'https://api.apiopen.top',//API请求地址
    baseURL : 'http://localhost:5005',//API请求地址
    timeout : 5000//超时时间 5S
})


var emlLoading;
let serviceTips = '服务器超时';
//axios 拦截器
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    //Loading 动画开启
    emlLoading = eml.Loading.service({
        lock: true,
        text: 'Loading……',
        spinner: 'el-icon-loading',
        background: 'rgba(0, 0, 0, 0.7)'
    });
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    //Loading 动画关闭
    emlLoading.close();
    return response;
}, function (error) {
    // 对响应错误做点什么
    if (error.response) {
        switch (error.response.status) {
            //登录超时
            case 401:
                error.response.data = '登录超时，重新登录'
                router.replace({
                    path: '../login',
                    query: {
                        redirect: router.currentRoute.fullPath
                    }
                })
                break;
            case 404:
                error.response.data = '资源不存在'
                break;
            case 406:
                error.response.data = '头部无携带token'
                break;
            case 412:
                // 拦截错误 并重新跳入登页重新获取token
                router.replace({
                    path: '/login',
                    query: {
                        redirect: router.currentRoute.fullPath
                    } // 登录成功后跳入浏览的当前页面
                })
                error.response.data = '秘钥失效'
                localStorage.removeItem('tokendata') // 清除token
                break
            default:
                break;
        }
        serviceTips = error.response.data
    }
    Message.error(serviceTips)
    return Promise.reject(serviceTips);
});
export default ServiceReq