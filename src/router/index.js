import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

const recom = {
  template: '<router-view></router-view>'
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'helloWorld',
      component: () => import("../components/Apibusiness/APIList.vue"),
      hidden: true,//控制显示
      icon:"el-icon-menu",
      meta: {
        name: "控制器"
      }
    },
    {
      path: '/api',
      name: 'Api',
      component: recom,
      hidden: true,
      icon:"el-icon-share",
      meta: {
        name: 'API管理'
      },
      children: [
        {
          path: '/APIList',
          name: 'apiList',
          hidden: true,
          icon:"el-icon-tickets",
          meta: {
            name: 'API列表'
          },
          component: () => import("../components/Apibusiness/APIList.vue")
        },
        {
          path: '/APIEdit',
          name: 'apiedit',
          hidden: false,
          icon:"el-icon-tickets",
          meta: {
            name: 'API详情'
          },
          component: () => import("../components/Apibusiness/APIEdit.vue")
        },
        {
          path: '/Statuscode',
          name: 'statuscode',
          hidden: false,
          icon:"el-icon-document",
          meta: {
            name: '状态码管理'
          },
          component: () => import("../components/Apibusiness/APIList.vue")
        }
      ]
    }
  ]
})
